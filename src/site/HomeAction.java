package site;

import net.sourceforge.stripes.action.*;

/**
 * Author : Efe
 * Date Created : 4/24/16
 */
@UrlBinding("/")
public class HomeAction implements ActionBean {
    private static final String VIEW = "/web/site/home.jsp";
    private ActionBeanContext context;

    @Override
    public ActionBeanContext getContext() {
        return this.context;
    }

    @Override
    public void setContext(ActionBeanContext ctx) {
        this.context = ctx;
    }

    //todo - Move to Child ActionBean class, Action, that all ActionBeans will extend.
    public String getBootstrap(){
        return "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css";
    }
    public String getStylesheet(){
        return "/web/css/home.css";
    }
    public String getLogo(){
        return "/web/assets/logo.png";
    }
    public String getJquery(){
        return "https://code.jquery.com/jquery.js";
    }
    public String getJavaScript(){
        return "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js";
    }

    //todo - These getters for links is very dirty, luckily it's temporary!
    public String getCarouselExample1(){
        return "/web/assets/carousel-sample-1.jpg";
    }

    public String getCarouselExample1Caption(){
        return "";
    }

    public String getThumbnailExample1(){
        return "/web/assets/thumbnail-example-1.jpg";
    }

    public String getCarouselExample2(){
        return "/web/assets/carousel-sample-2.jpg";
    }

    public String getCarouselExample2Caption(){
        return "";
    }

    public String getThumbnailExample2(){
        return "/web/assets/thumbnail-example-2.jpg";
    }

    public String getCarouselExample3(){
        return "/web/assets/carousel-sample-3.jpg";
    }

    public String getCarouselExample3Caption(){
        return "";
    }

    public String getThumbnailExample3(){
        return "/web/assets/thumbnail-example-3.jpg";
    }

    public String getCarouselExample4(){
        return "/web/assets/carousel-sample-4.jpg";
    }

    public String getCarouselExample4Caption(){
        return "Orion Space Craft Rocket Delta IV 4";
    }

    @DefaultHandler
    public Resolution show() {
        return new ForwardResolution(VIEW);
    }
}