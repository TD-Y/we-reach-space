<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld" %>

<%--@elvariable id="actionBean" type="site.HomeAction"--%>
<stripes:layout-definition>
    <html>
        <head>
            <title>We Reach Space</title>
            <meta name="viewport" content="width = device-width, initial-scale = 1.0">

            <!-- Bootstrap -->
            <link href="${actionBean.bootstrap}" rel="stylesheet">
            <!-- css files -->
            <link href="${actionBean.stylesheet}" rel="stylesheet">

            <!--[if lt IE 9]>
            <script src = "https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src = "https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
            <![endif]-->

            <stripes:layout-component name="head">
                <title>We Reach Space</title>
            </stripes:layout-component>
        </head>

        <body>
            <stripes:layout-component name="header">
                <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div class="navbar-header">
                        <stripes:link id="navHeader" class="navbar-brand active" beanclass="site.HomeAction">WE REACH SPACE</stripes:link>
                        <img id="logo" alt="brand" src="${actionBean.logo}">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse nav-collapse">
                        <div class="text-center">
                            <ul class="nav navbar-nav">
                                <li>
                                    <a href="#">Mission</a>
                                </li>
                                <li>
                                    <a href="#">Challenges</a>
                                </li>
                                <li>
                                    <a href="#">Projects</a>
                                </li>
                                <li>
                                    <a href="#">Resources</a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right" style="padding-right: 35px;">
                                <li>
                                    <a href="#">Login</a>
                                </li>
                                <li>
                                    <a href="#">Sign Up</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </stripes:layout-component>

            <stripes:layout-component name="contents">
            </stripes:layout-component>

            <stripes:layout-component name="footer">
            </stripes:layout-component>
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src = "${actionBean.jquery}"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script type="text/javascript" src = "${actionBean.javaScript}"></script>
        </body>
    </html>
</stripes:layout-definition>
