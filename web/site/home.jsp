<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld"%>

<%--@elvariable id="actionBean" type="site.HomeAction"--%>
<stripes:layout-render name="layout/defaultLayout.jsp">
    <stripes:layout-component name="contents">
        <!--todo: Create css file to remove repetitive styling-->
        <div class="row" style="background-color: black; padding-top: 65px;">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="alert alert-warning alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                    <Strong>Notice:</Strong> We Reach Space is under development.
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row" style="background-color: black;">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="alert alert-info alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
                    <Strong>Looking for our source code?</Strong>
                    <a href="https://bitbucket.org/TD-Y/we-reach-space/">Click me!</a>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row" style="background-color: black;">
            <div class="col-md-1"></div>
            <!-- Featured pictures/videos -->
            <div class="col-md-10">
                <div id="featured-carousel" class="carousel slider" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#featured-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#featured-carousel" data-slide-to="1"></li>
                        <li data-target="#featured-carousel" data-slide-to="2"></li>
                        <li data-target="#featured-carousel" data-slide-to="3"></li>
                    </ol>
                    <!-- Wrapper for slide -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <a href="http://www.nasa.gov/content/nasas-journey-to-mars">
                                <img class="carousel-pic" src="${actionBean.carouselExample1}" alt="0">
                                <div class="carousel-caption">
                                    <p>${actionBean.carouselExample1Caption}</p>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="http://www.nasa.gov/press-release/nasa-to-announce-mars-mystery-solved">
                                <img class="carousel-pic" src="${actionBean.carouselExample2}" alt="1">
                                <div class="carousel-caption">
                                    <p>${actionBean.carouselExample2Caption}</p>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="https://www.nasa.gov/press/2014/october/nasa-prepares-its-science-fleet-for-oct-19-mars-comet-encounter/">
                                <img class="carousel-pic" src="${actionBean.carouselExample3}" alt="2">
                                <div class="carousel-caption">
                                    <p>${actionBean.carouselExample3Caption}</p>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="http://mars.nasa.gov/participate/send-your-name/orion-first-flight/learn/">
                                <img class="carousel-pic" src="${actionBean.carouselExample4}" alt="3">
                                <div class="carousel-caption">
                                    <p>${actionBean.carouselExample4Caption}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control" href="#featured-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#featured-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <!-- Featured Projects -->
        <div class="row" style="margin-top:35px;">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="text-center panel-heading">
                        <h5 class="panel-title">Featured Projects</h5>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <img class="featured-project" src="${actionBean.thumbnailExample1}" alt="example-1">
                                    <div class="caption">
                                        <h3 class="text-center">Example Project 1</h3>
                                        <p class="text-center">
                                            This is an example of a project thumbnail. It will display a photo above and
                                            it will have two buttons at the bottom. One to contribute, and one for some
                                            other neat idea!
                                        </p>
                                        <h4>Progress:</h4>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="13" aria-valuemin="0" aria-valuemax="100" style="width: 13%;">
                                                13%
                                            </div>
                                        </div>
                                        <h4>Support:</h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="1237" aria-valuemin="0" aria-valuemax="10000" style="min-width: 5em; width: 12.37%">
                                                1,237
                                            </div>
                                        </div>
                                        <p>
                                            <a href="#" class="btn btn-sm btn-primary" role="button">Contribute</a>
                                            <a href="#" class="btn btn-sm btn-info" role="button">Like</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <img class="featured-project" src="${actionBean.thumbnailExample2}" alt="example-2">
                                    <div class="caption">
                                        <h3 class="text-center">Example Project 2</h3>
                                        <p class="text-center">
                                            This is an example of a project thumbnail. It will display a photo above and
                                            it will have two buttons at the bottom. One to contribute, and one for some
                                            other neat idea!
                                        </p>
                                        <h4>Progress:</h4>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="29" aria-valuemin="0" aria-valuemax="100" style="width: 29%;">
                                                29%
                                            </div>
                                        </div>
                                        <h4>Support:</h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="6958" aria-valuemin="0" aria-valuemax="10000" style="width: 69.58%">
                                                6,958
                                            </div>
                                        </div>
                                        <p>
                                            <a href="#" class="btn btn-sm btn-primary" role="button">Contribute</a>
                                            <a href="#" class="btn btn-sm btn-info" role="button">Like</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <img class="featured-project" src="${actionBean.thumbnailExample3}" alt="example-3">
                                    <div class="caption">
                                        <h3 class="text-center">Example Project 3</h3>
                                        <p class="text-center">
                                            This is an example of a project thumbnail. It will display a photo above and
                                            it will have two buttons at the bottom. One to contribute, and one for some
                                            other neat idea!
                                        </p>
                                        <h4>Progress:</h4>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;">
                                               65%
                                            </div>
                                        </div>
                                        <h4>Support:</h4>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="352" aria-valuemin="0" aria-valuemax="10000" style="min-width: 4em; width: 3.52%">
                                                352
                                            </div>
                                        </div>
                                        <p>
                                            <a href="#" class="btn btn-sm btn-primary" role="button">Contribute</a>
                                            <a href="#" class="btn btn-sm btn-info" role="button">Like</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <nav class="text-center">
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <!-- Challanges -->
        <div id="challenges" class="row" style="background-color: black">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <h1 class="text-center" style="color: ghostwhite;">Featured Challenges</h1>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10" style="margin-top:35px;">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <a href="http://www.nasa.gov/directorates/spacetech/centennial_challenges/sample_return_robot/index.html">
                                    <img class="img-rounded featured-challenge" src="http://www.nasa.gov/sites/default/files/thumbnails/image/srr16_registration_slide.jpeg" alt="example-challenge-1"/>
                                    <h3 class="text-center">Sample Return Robot</h3>
                                </a>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <a href="http://www.nasa.gov/directorates/spacetech/centennial_challenges/3DPHab/index.html">
                                    <img class="img-rounded featured-challenge" src="http://www.nasa.gov/sites/default/files/styles/side_image/public/thumbnails/image/3d-printed-habitat-challenge-shareable-for-release.jpg?itok=YBkWfIg3" alt="example-challenge-2"/>
                                    <h3 class="text-center">3-D Printed Habitat</h3>
                                </a>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-top: 35px;">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <a href="http://www.nasa.gov/directorates/spacetech/centennial_challenges/cubequest/index.html">
                                    <img class="img-rounded featured-challenge" src="http://www.nasa.gov/sites/default/files/styles/ubernode_alt_horiz/public/thumbnails/image/cubequest_challenge_moon_in_front.jpeg?itok=BQxCUwG9" alt="example-challenge-3"/>
                                    <h3 class="text-center">Cube Quest</h3>
                                </a>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <a href="https://2016.spaceappschallenge.org/challenges">
                                    <img class="img-rounded featured-challenge" src="https://s3.amazonaws.com/files.open.nasa.gov/images/SpaceApps_logo-circle-white.width-500.png" alt="example-challenge-4"/>
                                    <h3 class="text-center">Space Apps</h3>
                                </a>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row" style="background-color: black; padding-top: 15px;">
            <div class="col-md-12"></div>
        </div>
    </stripes:layout-component>
</stripes:layout-render>